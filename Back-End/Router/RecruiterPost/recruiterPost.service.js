const { PrismaClient } = require("@prisma/client");
const gateToken = require("../../Middleware/Middleware");
const moment = require("moment-timezone");

moment().tz("Asia/Ho_Chi_Minh").format();

BigInt.prototype.toJSON = function () {
	return this.toString();
};

const prisma = new PrismaClient({
	// log: [
	// 	{
	// 		emit: "event",
	// 		level: "query",
	// 	},
	// ],
});
prisma.$on("query", (e) => {
	console.log("Query: " + e.query);
	console.log("Params: " + e.params);
	console.log("Duration: " + e.duration + "ms");
});

function exclude(obj, keys) {
	for (let key of keys) {
		delete obj[key];
	}
	return obj;
}

function arrayIntersection(arr1, arr2) {
	return arr1.filter((x) => arr2.includes(x));
}

function arrayDifference(arr1, arr2) {
	return arr1.filter((x) => !arr2.includes(x));
}

function getDateLast(last) {
	const now = new Date();

	return new Date(
		now.getFullYear(),
		now.getMonth(),
		now.getDate() - Number(last)
	);
}

const getListPostOfUser = async (req, res) => {
	let { key_word, item_per_page = 10, page = 1 } = req.query;
	try {
		let { user_id = null, user_type_id = null } = req;
		if (!user_id) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Người dùng không hợp lệ",
			});
		}
		let resultListPost = await prisma.Recruitment_Post.findMany({
			where: {
				AND: [
					{
						recuiter_id: Number(user_id),
						// is_active: true,
						is_delete: false,
						title: { contains: key_word },
					},
				],
			},
			take: Number(item_per_page),
			skip: Number(item_per_page * (page - 1)),
		});
		let total = await prisma.Recruitment_Post.count({
			where: {
				AND: [
					{
						recuiter_id: Number(user_id),
						// is_active: true,
						is_delete: false,
					},
				],
			},
		});

		if (resultListPost && resultListPost.length < 0) {
			return res.json({
				code: 400,
				message: "Bạn chưa có danh sách bài đăng tuyển dụng",
				status_resposse: false,
			});
		}
		return res.json({
			code: 200,
			message: "Lấy danh sách bài đăng truyển dụng thành công",
			status_resposse: true,
			data: {
				result: resultListPost,
				total: total,
			},
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};
const getListPostOfUserById = async (req, res) => {
	try {
		let { key_word, item_per_page = 10, page = 1 } = req.query;
		let { user_id = null } = req.query;
		if (!user_id) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Người dùng không hợp lệ",
			});
		}
		let resultListPost = await prisma.Recruitment_Post.findMany({
			where: {
				AND: [
					{
						recuiter_id: Number(user_id),
						// is_active: true,
						is_delete: false,
						title: { contains: key_word },
					},
				],
			},
			take: Number(item_per_page),
			skip: Number(item_per_page * (page - 1)),
		});

		let total = await prisma.Recruitment_Post.count({
			where: {
				AND: [
					{
						recuiter_id: Number(user_id),
						// is_active: true,
						is_delete: false,
					},
				],
			},
		});

		if (resultListPost && resultListPost.length < 0) {
			return res.json({
				code: 400,
				message: "Bạn chưa có danh sách bài đăng tuyển dụng",
				status_resposse: false,
			});
		}
		return res.json({
			code: 200,
			message: "Lấy danh sách bài đăng truyển dụng thành công",
			status_resposse: true,
			data: {
				result: resultListPost,
				total: total,
			},
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};
const getListPost = async (req, res) => {
	let {
		key_word,
		item_per_page = 10,
		page = 1,
		sort_by = "create_date",
		sort_order = "desc",
		from_value,
		to_value,
		gender,
		province_code,
		district_code,
		ward_code,
		list_job_type,
		list_major,
		date_post,
		is_content = false,
	} = req.query;
	try {
		const list_job_type_array = list_job_type
			? list_job_type.split(",").map(Number)
			: undefined;
		const list_major_array = list_major
			? list_major.split(",").map(Number)
			: undefined;

		const andArr = [
			{
				is_active: {
					equals: true,
				},
			},
			{
				is_delete: {
					equals: false,
				},
			},
			{
				title: { contains: key_word },
			},
			{
				from_value: {
					gte: from_value && Number(from_value),
				},
			},
			{
				to_value: {
					lte: to_value && Number(to_value),
				},
			},
			{
				gender: gender && Number(gender),
			},
			{
				province_code: { contains: province_code },
			},
			{
				district_code: { contains: district_code },
			},
			{
				ward_code: { contains: ward_code },
			},
			{
				create_date: {
					gte: date_post && getDateLast(date_post),
				},
			},
			{
				post_job_types: {
					some: {
						job_type_id: {
							in: list_job_type_array,
						},
						is_active: true,
						is_delete: false,
					},
				},
			},
			{
				post_majors: {
					some: {
						majors_id: {
							in: list_major_array,
						},
						is_active: true,
						is_delete: false,
					},
				},
			},
		];
		const sortArr = [];
		if (sort_by === "create_date") {
			sortArr.push({
				create_date: sort_order,
			});
		}
		if (sort_by === "id") {
			sortArr.push({
				id: sort_order,
			});
		}

		let resultList = await prisma.Recruitment_Post.findMany({
			where: {
				AND: andArr,
			},
			orderBy: sortArr,
			take: Number(item_per_page),
			skip: Number(item_per_page * (page - 1)),
			include: {
				user: {
					select: {
						id: true,
						full_name: true,
						user_type_id: true,
						email: true,
						number_phone: true,
						logo: true,
						address: true,
						province_code: true,
						district_code: true,
						ward_code: true,
						avartar: true,
					},
				},
				post_job_types: {
					select: {
						id: true,
						job_type: {
							select: {
								id: true,
								job_type_name: true,
							},
						},
					},
					where: {
						is_delete: false,
						is_active: true,
					},
				},
				post_majors: {
					select: {
						id: true,
						majors: {
							select: {
								id: true,
								majors_name: true,
							},
						},
					},
					where: {
						is_delete: false,
						is_active: true,
					},
				},
				provinces: {
					select: {
						id: true,
						code: true,
						name: true,
						name_en: true,
						full_name: true,
						full_name_en: true,
						code_name: true,
					},
				},
				districts: {
					select: {
						id: true,
						code: true,
						name: true,
						name_en: true,
						full_name: true,
						full_name_en: true,
						code_name: true,
					},
				},
				wards: {
					select: {
						id: true,
						code: true,
						name: true,
						name_en: true,
						full_name: true,
						full_name_en: true,
						code_name: true,
					},
				},
			},
		});
		let total = await prisma.Recruitment_Post.count({
			where: {
				AND: andArr,
			},
		});
		if (resultList && resultList.length < 0) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Lấy danh sách thất bại",
			});
		}
		const finalResult = !is_content
			? resultList.map((result) => exclude(result, ["content"]))
			: resultList;
		return res.json({
			code: 200,
			message: "Lấy danh sách thành công",
			status_resposse: true,
			data: {
				result: finalResult,
				total: total,
			},
		});
	} catch (error) {
		// console.log(error);
		// throw new Error(error);
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

const createPost = async (req, res) => {
	try {
		let {
			content,
			recuiter_id,
			to_value,
			from_value,
			title,
			is_active,
			is_delete,
			gender,
			province_code,
			district_code,
			ward_code,
			address,
			list_job_type = [],
			list_major = [],
		} = req.body;
		let { user_id = null, user_type_id = null } = req;
		if (!user_id) {
			return res.json({
				code: 400,
				message: "Người dùng không hợp lệ",
				status_resposse: false,
			});
		}
		let list_job_type_array = list_job_type
			? list_job_type.map(({ job_type_id }) => {
					return {
						is_delete: false,
						is_active: true,
						job_type: {
							connect: {
								id: Number(job_type_id),
							},
						},
					};
			  })
			: [];

		let list_major_array = list_major
			? list_major.map(({ majors_id }) => {
					return {
						is_delete: false,
						is_active: true,
						majors: {
							connect: {
								id: Number(majors_id),
							},
						},
					};
			  })
			: [];

		const result = await prisma.Recruitment_Post.create({
			data: {
				content: content,
				title: title,
				recuiter_id: Number(user_id),
				to_value: Number(to_value),
				from_value: Number(from_value),
				province_code: province_code,
				district_code: district_code,
				ward_code: ward_code,
				address: address,
				create_date: new Date(moment(new Date()).format("YYYY-MM-DD HH:mm:ss")),
				gender: Number(gender),
				is_active: is_active,
				is_delete: is_delete,
				post_job_types: {
					create: list_job_type_array,
				},
				post_majors: {
					create: list_major_array,
				},
			},
		});

		if (!result) {
			return res.json({
				code: 400,
				status_resposse: false,
				messsage: "Tạo bài đăng thất bại",
			});
		}
		return res.json({
			code: 200,
			message: "Tạo bài đăng thành công",
			status_resposse: true,
			data: result,
		});
	} catch (error) {
		console.log(error);
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

const update = async (req, res) => {
	try {
		let {
			post_id,
			content,
			recuiter_id,
			to_value,
			from_value,
			title,
			is_active,
			is_delete,
			gender,
			province_code,
			district_code,
			ward_code,
			address,
			list_job_type = [],
			list_major = [],
		} = req.body;
		let { user_id = null, user_type_id = null } = req;
		let isExists = await prisma.Recruitment_Post.findFirst({
			where: {
				AND: [
					{
						id: Number(post_id),
						// is_active: is_active,
						is_delete: is_delete,
						recuiter_id: Number(user_id),
					},
				],
			},
			include: {
				post_job_types: {
					select: {
						id: true,
						job_type: {
							select: {
								id: true,
								job_type_name: true,
							},
						},
					},
					where: {
						is_delete: false,
						is_active: true,
					},
				},
				post_majors: {
					select: {
						id: true,
						majors: {
							select: {
								id: true,
								majors_name: true,
							},
						},
					},
					where: {
						is_delete: false,
						is_active: true,
					},
				},
			},
		});
		if (!isExists) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Không tìm thấy bài đăng này",
			});
		}
		//major processing
		const oldMajor = isExists.post_majors.map((post_majors) =>
			Number(post_majors.majors.id)
		);
		const newMajor = list_major.map((item) => Number(item.majors_id));
		const deleteMajor = arrayDifference(oldMajor, newMajor);
		const updateOrCreateMajor = arrayDifference(newMajor, oldMajor);

		const upsertMajor = updateOrCreateMajor.map((majors_id) => {
			return {
				where: {
					post_id_majors_id: {
						majors_id: Number(majors_id),
						post_id: Number(post_id),
					},
				},
				update: {
					is_delete: false,
					is_active: true,
				},
				create: {
					is_delete: false,
					is_active: true,
					majors: {
						connect: {
							id: Number(majors_id),
						},
					},
				},
			};
		});
		//end major processing
		//job type processing
		const oldJobType = isExists.post_job_types.map((post_job_type) =>
			Number(post_job_type.job_type.id)
		);
		const newJobType = list_job_type.map((item) =>
			Number(item.job_type_id)
		);
		const deleteJobType = arrayDifference(oldJobType, newJobType);
		const updateOrCreateJobType = arrayDifference(newJobType, oldJobType);
		const upsertJobType = updateOrCreateJobType.map((job_type_id) => {
			return {
				where: {
					post_id_job_type_id: {
						job_type_id: Number(job_type_id),
						post_id: Number(post_id),
					},
				},
				update: {
					is_delete: false,
					is_active: true,
				},
				create: {
					is_delete: false,
					is_active: true,
					job_type: {
						connect: {
							id: Number(job_type_id),
						},
					},
				},
			};
		});
		//end job type processing

		const resultUpdate = await prisma.Recruitment_Post.update({
			where: {
				id: Number(post_id),
			},
			data: {
				title: title,
				content: content,
				to_value: to_value && Number(to_value),
				from_value: from_value && Number(from_value),
				update_date: new Date(moment(new Date()).format("YYYY-MM-DD")),
				update_user: user_id,
				gender: gender && Number(gender),
				province_code: province_code,
				district_code: district_code,
				ward_code: ward_code,
				address: address,
				is_active: is_active,
				post_job_types: {
					updateMany: {
						where: {
							job_type_id: { in: deleteJobType },
							post_id: Number(post_id),
						},
						data: {
							is_delete: true,
							is_active: false,
						},
					},
					upsert: upsertJobType,
				},
				post_majors: {
					updateMany: {
						where: {
							majors_id: { in: deleteMajor },
							post_id: Number(post_id),
						},
						data: {
							is_delete: true,
							is_active: false,
						},
					},
					upsert: upsertMajor,
				},
			},
		});

		if (!resultUpdate) {
			return res.json({
				code: 400,
				status_resposse: false,
				messsage: "Cập nhật bài đăng thất bại",
			});
		}

		return res.json({
			code: 200,
			message: "Cập nhật bài đăng thành công",
			status_resposse: true,
			data: resultUpdate,
		});
	} catch (error) {
		console.log(error);
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

const deletePost = async (req, res) => {
	try {
		let { post_id } = req.body;
		let { user_id = null, user_type_id = null } = req;
		let isExists = await prisma.Recruitment_Post.findFirst({
			where: {
				AND: [
					{
						id: Number(post_id),
						is_active: true,
						is_delete: false,
						recuiter_id: Number(user_id),
					},
				],
			},
		});
		if (!isExists) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Không tìm thấy bài đăng này",
			});
		}

		const resultDelete = await prisma.Recruitment_Post.update({
			where: {
				id: Number(post_id),
			},
			data: {
				is_active: false,
				is_delete: true,
				delete_date: new Date(moment(new Date()).format("YYYY-MM-DD")),
				delete_user: user_id,
				History_Apply_Job: {
					updateMany: {
						where: {
							post_id: Number(post_id),
						},
						data: {
							is_delete: true,
							is_active: false,
						},
					},
				},
			},
		});
		if (!resultDelete) {
			return res.json({
				code: 400,
				status_resposse: false,
				messsage: "Xóa bài viết thất bại",
			});
		}

		return res.json({
			code: 200,
			status_resposse: true,
			data: resultDelete,
			message: "Xóa bài đăng thành công",
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

const getDetail = async (req, res) => {
	try {
		let { post_id } = req.query;
		let isExists = await prisma.Recruitment_Post.findFirst({
			where: {
				AND: [
					{
						id: Number(post_id),
						// is_active: true,
						is_delete: false,
					},
				],
			},
			include: {
				user: {
					select: {
						id: true,
						full_name: true,
						user_type_id: true,
						email: true,
						number_phone: true,
						logo: true,
						address: true,
						province_code: true,
						district_code: true,
						ward_code: true,
						avartar: true,
					},
				},
				post_job_types: {
					select: {
						id: true,
						job_type: {
							select: {
								id: true,
								job_type_name: true,
							},
						},
					},
					where: {
						is_delete: false,
						is_active: true,
					},
				},
				post_majors: {
					select: {
						id: true,
						majors: {
							select: {
								id: true,
								majors_name: true,
							},
						},
					},
					where: {
						is_delete: false,
						is_active: true,
					},
				},
				provinces: {
					select: {
						id: true,
						code: true,
						name: true,
						name_en: true,
						full_name: true,
						full_name_en: true,
						code_name: true,
					},
				},
				districts: {
					select: {
						id: true,
						code: true,
						name: true,
						name_en: true,
						full_name: true,
						full_name_en: true,
						code_name: true,
					},
				},
				wards: {
					select: {
						id: true,
						code: true,
						name: true,
						name_en: true,
						full_name: true,
						full_name_en: true,
						code_name: true,
					},
				},
			},
		});
		if (!isExists) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Không tìm thấy bài đăng này",
			});
		}

		return res.json({
			code: 200,
			message: "Lấy danh sách bài đăng truyển dụng thành công",
			status_resposse: true,
			data: isExists,
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

const bookMarkPost = async (req, res) => {
	try {
		let { post_id } = req.body;
		let { user_id, user_type_id = null } = req;

		// Kiểm tra xem bài viết có tồn tại hay không
		let isExistsPost = await prisma.recruitment_Post.findFirst({
			where: {
				AND: [
					{
						is_active: true,
						is_delete: false,
						id: Number(post_id),
					},
				],
			},
		});
		if (!isExistsPost) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Bài viết này không tồn tại",
			});
		}

		// Kiểm tra xem người dùng đã yêu thích bài tuyển dụng này hay chưa
		let isExistLikePost = await prisma.recruitment_Post_User_Like.findFirst(
			{
				where: {
					AND: [
						{
							post_id: Number(post_id),
							user_id: Number(user_id),
						},
					],
				},
			}
		);
		// Nếu đã có tồn tại trong database thì check để cập nhật lại
		if (isExistLikePost) {
			let { is_active = true, is_delete = false } = isExistLikePost;
			if (is_active == true && is_delete == false) {
				return res.json({
					code: 200,
					status_resposse: true,
					message: "Bạn đã yêu thích bài đăng này",
				});
			} else {
				// Cập nhật lại nếu đã thích vài viết đó
				const resultUpdate =
					await prisma.recruitment_Post_User_Like.update({
						where: {
							id: Number(isExistLikePost.id),
						},
						data: {
							is_active: true,
							is_delete: false,
						},
						include: {
							user_account: {
								select: {
									id: true,
									full_name: true,
									user_type_id: true,
									email: true,
									number_phone: true,
									logo: true,
									address: true,
									province_code: true,
									district_code: true,
									ward_code: true,
								},
							},
							Recruitment_Post: {
								select: {
									id: true,
									title: true,
									post_job_types: {
										select: {
											id: true,
											job_type: {
												select: {
													id: true,
													job_type_name: true,
												},
											},
										},
										where: {
											is_delete: false,
											is_active: true,
										},
									},
									post_majors: {
										select: {
											id: true,
											majors: {
												select: {
													id: true,
													majors_name: true,
												},
											},
										},
										where: {
											is_delete: false,
											is_active: true,
										},
									},
								},
							},
						},
					});
				if (!resultUpdate) {
					return res.json({
						code: 400,
						status_resposse: false,
						message: "Yêu thích bài viết đã xảy ra lỗi !",
					});
				}
				return res.json({
					code: 200,
					status_resposse: true,
					data: resultUpdate,
					message: "Yêu thích bài viết thành công",
				});
			}
		}
		// Nếu chưa từng yêu thích bài viết sẽ tạo mới
		const resultCreate = await prisma.recruitment_Post_User_Like.create({
			data: {
				user_id: Number(user_id),
				post_id: Number(post_id),
				create_date: new Date(moment(new Date()).format("YYYY-MM-DD")),
				create_user: user_id,
				is_active: true,
				is_delete: false,
			},
			include: {
				user_account: {
					select: {
						id: true,
						full_name: true,
						user_type_id: true,
						email: true,
						number_phone: true,
						logo: true,
						address: true,
						province_code: true,
						district_code: true,
						ward_code: true,
					},
				},
				Recruitment_Post: {
					select: {
						id: true,
						title: true,
						post_job_types: {
							select: {
								id: true,
								job_type: {
									select: {
										id: true,
										job_type_name: true,
									},
								},
							},
							where: {
								is_delete: false,
								is_active: true,
							},
						},
						post_majors: {
							select: {
								id: true,
								majors: {
									select: {
										id: true,
										majors_name: true,
									},
								},
							},
							where: {
								is_delete: false,
								is_active: true,
							},
						},
					},
				},
			},
		});
		if (!resultCreate) {
			return res.json({
				code: 400,
				status_resposse: false,
				message:
					"Quá trình yêu thích bài đăng xảy ra vấn đề vui lòng thử lại sau",
			});
		}
		return res.json({
			code: 200,
			status_resposse: true,
			data: resultCreate,
			message: "Quá trình yêu thích thành công",
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

const unbookMarkPost = async (req, res) => {
	try {
		let { post_id } = req.body;
		let { user_id, user_type_id = null } = req;

		// Kiểm tra xem bài viết có tồn tại hay không
		let isExistsPost = await prisma.recruitment_Post.findFirst({
			where: {
				AND: [
					{
						is_active: true,
						is_delete: false,
						id: Number(post_id),
					},
				],
			},
		});
		if (!isExistsPost) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Bài viết này không tồn tại",
			});
		}

		// Kiểm tra xem người dùng đã yêu thích bài tuyển dụng này hay chưa
		let isExistLikePost = await prisma.recruitment_Post_User_Like.findFirst(
			{
				where: {
					AND: [
						{
							post_id: Number(post_id),
							user_id: Number(user_id),
							is_active: true,
							is_delete: false,
						},
					],
				},
			}
		);

		if (!isExistLikePost) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Không tìm thấy việc yêu thích bài đăng từ bạn",
			});
		}

		const resultUnBookMark = await prisma.recruitment_Post_User_Like.update(
			{
				where: {
					id: Number(isExistLikePost.id),
				},
				data: {
					is_active: false,
					is_delete: true,
					delete_date: new Date(
						moment(new Date()).format("YYYY-MM-DD")
					),
				},
				include: {
					user_account: {
						select: {
							id: true,
							full_name: true,
							user_type_id: true,
							email: true,
							number_phone: true,
							logo: true,
							address: true,
							province_code: true,
							district_code: true,
							ward_code: true,
						},
					},
					Recruitment_Post: {
						select: {
							id: true,
							title: true,
							post_job_types: {
								select: {
									id: true,
									job_type: {
										select: {
											id: true,
											job_type_name: true,
										},
									},
								},
								where: {
									is_delete: false,
									is_active: true,
								},
							},
							post_majors: {
								select: {
									id: true,
									majors: {
										select: {
											id: true,
											majors_name: true,
										},
									},
								},
								where: {
									is_delete: false,
									is_active: true,
								},
							},
						},
					},
				},
			}
		);
		if (!resultUnBookMark) {
			return res.json({
				code: 400,
				status_resposse: false,
				message: "Bỏ yêu thích bài đăng không thành công",
			});
		}
		return res.json({
			code: 200,
			status_resposse: true,
			message: "Bỏ yêu thích bài đăng thành công",
			data: resultUnBookMark,
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			messsage: error.message,
		});
	}
};

const getListBookMark = async (req, res) => {
	try {
		let {
			key_word,
			item_per_page = 10,
			page = 1,
			from_value,
			to_value,
			gender,
			is_content = false,
		} = req.query;
		let { user_id, user_type_id = null } = req;

		const result = await prisma.recruitment_Post_User_Like.findMany({
			where: {
				AND: [
					{
						is_active: { equals: true },
						is_delete: { equals: false },
						user_id: Number(user_id),
						user_account: {
							is_active: true,
							is_delete: false,
						},
						Recruitment_Post: {
							// is_active: true,
							is_delete: false,
						},
					},
				],
			},
			take: Number(item_per_page),
			skip: Number(item_per_page * (page - 1)),
			include: {
				user_account: {
					select: {
						id: true,
						full_name: true,
						user_type_id: true,
						email: true,
						number_phone: true,
						logo: true,
						address: true,
						province_code: true,
						district_code: true,
						ward_code: true,
					},
				},
				Recruitment_Post: {
					select: {
						id: true,
						content: is_content,
						title: true,
						post_job_types: {
							select: {
								id: true,
								job_type: {
									select: {
										id: true,
										job_type_name: true,
									},
								},
							},
							where: {
								is_delete: false,
								is_active: true,
							},
						},
						post_majors: {
							select: {
								id: true,
								majors: {
									select: {
										id: true,
										majors_name: true,
									},
								},
							},
							where: {
								is_delete: false,
								is_active: true,
							},
						},
					},
				},
			},
		});

		return res.json({
			code: 200,
			message: "Lấy dữ liệu thành công",
			status_resposse: true,
			data: result,
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

const getListBookMarkById = async (req, res) => {
	try {
		let {
			key_word,
			item_per_page = 10,
			page = 1,
			from_value,
			to_value,
			gender,
			is_content = false,
			user_id,
		} = req.query;
		let { user_type_id = null } = req;

		const result = await prisma.recruitment_Post_User_Like.findMany({
			where: {
				AND: [
					{
						is_active: { equals: true },
						is_delete: { equals: false },
						user_id: Number(user_id),
						user_account: {
							is_active: true,
							is_delete: false,
						},
						Recruitment_Post: {
							is_active: true,
							is_delete: false,
						},
					},
				],
			},
			take: Number(item_per_page),
			skip: Number(item_per_page * (page - 1)),
			include: {
				user_account: {
					select: {
						id: true,
						full_name: true,
						user_type_id: true,
						email: true,
						number_phone: true,
						logo: true,
						address: true,
						province_code: true,
						district_code: true,
						ward_code: true,
					},
				},
				Recruitment_Post: {
					select: {
						id: true,
						content: is_content,
						title: true,
						post_job_types: {
							select: {
								id: true,
								job_type: {
									select: {
										id: true,
										job_type_name: true,
									},
								},
							},
							where: {
								is_delete: false,
								is_active: true,
							},
						},
						post_majors: {
							select: {
								id: true,
								majors: {
									select: {
										id: true,
										majors_name: true,
									},
								},
							},
							where: {
								is_delete: false,
								is_active: true,
							},
						},
					},
				},
			},
		});

		return res.json({
			code: 200,
			message: "Lấy dữ liệu thành công",
			status_resposse: true,
			data: result,
		});
	} catch (error) {
		return res.json({
			code: 400,
			status_resposse: false,
			message: error.message,
		});
	}
};

module.exports = {
	update,
	createPost,
	getListPost,
	getListPostOfUser,
	deletePost,
	getDetail,
	getListPostOfUserById,
	bookMarkPost,
	unbookMarkPost,
	getListBookMark,
	getListBookMarkById,
};
