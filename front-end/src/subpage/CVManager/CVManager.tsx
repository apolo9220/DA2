import React, { useEffect, useState } from "react";
import "./CVManager.scss";
import { useModal } from "src/hooks/useModal";
import { useReduxDispatch, useReduxSelector } from "src/redux/redux-hook";
import { getListCV, selectCVList } from "src/redux/slice/CVSlide";
import { CV } from "src/types/Type";
import CVActions from "./CVActions/CVActions";
import ButtonCommon from "src/common/ButtonCommon/ButtonCommon";
import { ModalCommon, TableCommon } from "src/common";
import { ColumnCommon } from "src/common/TableCommon/TableCommon";
import ShowCVModal from "./ShowCVModal/ShowCVModal";
import AddCVModal from "./AddCVModal/AddCVModal";
import RenameCVModal from "./RenameCVModal/RenameCVModal";
import SuspenseLoading from "src/components/SuspenseLoading/SuspenseLoading";
type Props = {
	isHideAdd?: boolean;
	isHideTitle?: boolean;
	isHideAction?: boolean;
	isUseCustomData?: boolean;
	customData?: CV[];
};

const CVManager = ({
	isHideAction = false,
	isHideAdd = false,
	isHideTitle = false,
	isUseCustomData = false,
	customData = [],
}: Props) => {
	const { isOpen, close, open } = useModal(false);
	const {
		isOpen: isRenameOpen,
		close: renameClose,
		open: renameOpen,
	} = useModal(false);
	const {
		isOpen: isViewOpen,
		close: viewClose,
		open: viewOpen,
	} = useModal(false);
	const cvList = useReduxSelector(selectCVList);
	const [edited, setEdited] = useState<CV>();
	const [viewed, setViewed] = useState<CV>();
	const dispatch = useReduxDispatch();
	useEffect(() => {
		if (isUseCustomData) {
			return;
		}
		dispatch(getListCV({}));
	}, [dispatch, isUseCustomData]);
	useEffect(() => {}, [cvList]);

	const handleRenameClick = (record: CV) => {
		setEdited(record);
		renameOpen();
	};

	const handleViewPdf = (record: CV) => {
		setViewed(record);
		viewOpen();
	};

	return (
		<div className="cv-manager">
			<React.Suspense fallback={<SuspenseLoading size="medium" />}>
				{!isHideTitle && (
					<div className="dashboard-title">CV Manager</div>
				)}
				{!isHideAdd && (
					<div className="button-add-action">
						<ButtonCommon size="small" onClick={open}>
							Add cv
						</ButtonCommon>
					</div>
				)}
				<div className="inner-content">
					<TableCommon
						dataSource={isUseCustomData ? customData : cvList}
						rowKey="id"
					>
						<ColumnCommon
							title="CV Name"
							dataIndex="file_name"
							key="file_name"
						/>
						<ColumnCommon
							title="Create Date"
							dataIndex="create_date"
							key="create_date"
							render={(value: string) => {
								const event = new Date(value);

								return (
									<div>
										{event.toLocaleDateString("vi-VI")}
									</div>
								);
							}}
						/>
						{!isHideAction && (
							<ColumnCommon<CV>
								title="Action"
								key="action"
								width={"20%"}
								render={(_, record) => {
									return (
										<CVActions
											record={record}
											handleRenameClick={
												handleRenameClick
											}
											handleViewPdf={handleViewPdf}
										/>
									);
								}}
							/>
						)}
					</TableCommon>
				</div>
				<ModalCommon open={isOpen} onCancel={close} footer={null}>
					<AddCVModal onClose={close} />
				</ModalCommon>
				<ModalCommon
					centered
					open={isRenameOpen}
					onCancel={renameClose}
					footer={null}
				>
					<RenameCVModal onClose={renameClose} edited={edited} />
				</ModalCommon>
				<ModalCommon
					centered
					open={isViewOpen}
					onCancel={viewClose}
					footer={null}
					width="auto"
				>
					<ShowCVModal record={viewed} />
				</ModalCommon>
			</React.Suspense>
		</div>
	);
};

export default CVManager;
