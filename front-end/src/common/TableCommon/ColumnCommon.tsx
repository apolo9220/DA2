import { Table } from "antd";

const { Column: ColumnCommon } = Table;
export type ColumnCommonType = typeof ColumnCommon;
export default ColumnCommon;
