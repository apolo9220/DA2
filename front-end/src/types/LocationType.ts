export type LocationCode = {
	province_code?: string | number | null;
	district_code?: string | number | null;
	ward_code?: string | number | null;
};
