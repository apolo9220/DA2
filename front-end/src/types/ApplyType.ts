export type ApplyParameter = {
	cv_id: number;
	post_id: number;
};

export type UnApplyParameter = {
	cv_id: number;
	post_id: number;
};
