import { PaginationParameters } from "./UtilType";

export interface GetListAccountTypeParameters extends PaginationParameters {
	key_word?: string;
}
